# Preface

Searching for an engineer's job shows many distinctions on terms of what kind of people they are looking for: they can be looking for a junior engineers, senior engineers, practitioners or just engineers. This makes searching for a job a complex duty because there isn't a clear definition about what is a junior engineer and a senior one. The most common definition is than an engineer is senior when they complete 5 years on the same position. What made it a Senior Engineer in that particular area? And is that enough?

One thing I've seen in my road to be a senior engineer is that there are tons of features that are desirable in an engineer, but the most important thing is that all the features need to be developed in parallel: it isn't worthy to have 1000 hours of training without experience and in the same way 1000 hours of doing the same task aren't worthy. That's why one of the things I will describe will be time management.

In "Road to Senior", I will show you my vision about what is a *Senior Engineer* and some answers to the following FAQ:

- How many years of experience are enough?
- What knowledge is needed to become a Senior Engineer?
- Can you become a Senior Engineer in less than 10 years?
- Does a Senior Engineer need to be in the same organization?
- How much should a Senior Engineer earn?

It's also very important to clarify that my point of view is oriented mostly to system engineering and similar.

1. Computer science
1. Computing engineer
1. IT engineer

Finally, you need to understand than this isn't a step-by-step tutorial. It is more a guide that I hope serves you to take you to the engineering in your field to the maximum and helps you grow into the beautiful experience of the Engineer career.
