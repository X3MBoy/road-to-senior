# Road To Senior

## English

_"Road To Senior"_ is a book about the vision of the author on the things that needs to be done to became a senior engineer.

### Authors and acknowledgment
Eduard _"x3mboy"_ Lucena

### License
This work is licensed under the terms of CC BY-SA 4.0

### Project status
This project is a Work In Progress and it's not complete to produce a full book yet.

## Spanish

_"El camino para convertirse en un Ingeniero Senior"_ es un libro sobre la visión del autor sobre las cosas que son necesarias para convertirse en un ingeniero senior.

### Authors and acknowledgment
Eduard _"x3mboy"_ Lucena

### Licencia
Este trabajo se encuentra licenciado bajo los términos de la licencia: CC BY-SA 4.0. La licencia (archivo LICENSE) se encuentra en inglés, pero los términos en español pueden ser consultados en el sitio de [Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/deed.es)

### Estatus del Proyecto
Este proyecto es un _"Trabajo en Progreso (WIP)"_ y no está completo para producir un libro aún.
