# Introducción

Las búsquedas laborales de ingenieros están llenas de distinciones en cuanto al tipo de personal que buscan: pueden buscar ingenieros junior, ingenieros senior, practicantes o simplemente ingenieros. Esto hace que buscar un trabajo se convierta en una labor compleja, dado que no hay una definición clara de que es un ingeniero junior y uno senior. La definición más común en las organizaciones es que un ingeniero es senior al completar 5 años de experiencia en un mismo cargo, lo que lo convierte en Senior en esa área en particular, pero ¿es esto suficiente?

Una cosa que he visto en mi camino a ser un ingeniero senior es que hay muchas características que son deseadas en un ingeniero, pero que lo más importante es que esas carácterísticas sean llevadas de la mano unas con otras: no vale de nada tener 1000 horas de entrenamiento teórico sin experiencia práctica, y de igual manera, 1000 horas de práctica no son de mayor utilidad si son haciendo la misma tarea una y otra vez. Es por ello que entre las habilidades que describiré durante el libro es la adminstración del tiempo.

En "El camino para convertirse en un Ingeniero Senior", les describiré mi visión sobre que es un ingeniero senior y algunas preguntas muy comunes como:

- ¿Los años de experiencia bastan?
- ¿Que cosas debe saber un ingeniero senior?
- ¿Se puede ser ingeniero senior en menos de 10 años?
- ¿Un ingeniero senior tiene que estar siempre en la misma organización?
- ¿Cuanto debe ganar un ingeniero senior?

Es muy importante aclarar que este punto de vista está sobretodo enfocado en la ingeniería en sistemas y sus similares:

1. Ingenieria en sistemas
1. Ingeniería en computación
1. Ingeniería en informática
1. Ingeniería en sistemas de información
1. Ingeniería civil en informática

Finalmente, es necesario entender que esto no es un manual paso a paso, es más bien una guía que espero les sirva para llevar el camino de la ingeniería en su especialidad a su máximo exponente y puedan crecer dentro de la hermosa carrera de ingeniería.

## ¿Qué significa ser un Ingeniero Junior?

Un ingeniero Junior es un ingeniero que recién comienza sus labores en cualquier área de ingeniería. En el área de informática existen muchos cargos que no requieren de estudios académicos formales, es decir el estudio de la ingeniería,
